import { Routes, Route } from "react-router-dom";

import { useEffect, useState } from "react";
import { StreamChat } from "open-chat-js";
import { Chat, enTranslations, Streami18n } from 'open-chat-react';

import { getRandomImage } from "./assets";
import { useChecklist } from "./ChecklistTasks";
import { ChannelContainer } from "./components/ChannelContainer/ChannelContainer";
import { Sidebar } from "./components/Sidebar/Sidebar";

import { WorkspaceController } from "./context/WorkspaceController";

import type { StreamChatType } from "./types";
import { getToken, getUserInfo } from "./utils/useHttp";
import { useNavigate } from "react-router-dom";

import { Button, Col, Input, Row, notification } from "antd";
import { saveUserInfo, useHttp, saveToken } from "./utils/useHttp";

const urlParams = new URLSearchParams(window.location.search);

const apiKey = urlParams.get("apikey") || process.env.REACT_APP_STREAM_KEY;

let user = getUserInfo();
let userToken = getToken();
const theme = urlParams.get("theme") || "light";
const targetOrigin =
  urlParams.get("target_origin") || process.env.REACT_APP_TARGET_ORIGIN;

const i18nInstance = new Streami18n({
  language: "en",
  translationsForLanguage: {
    ...enTranslations,
  },
});

const ChatPage = () => {
  useEffect(() => {
    if (!user || !userToken) {
      window.location.href = "/";
    }
  }, []);

  const client = StreamChat.getInstance<StreamChatType>(apiKey!, {
    enableInsights: true,
    enableWSFallback: true,
  });
  client.connectUser(
    { id: user!, name: user, image: getRandomImage() },
    userToken!
  );
  useEffect(() => {
    const handleColorChange = (color: string) => {
      const root = document.documentElement;
      if (color.length && color.length === 7) {
        root.style.setProperty("--primary-color", `${color}E6`);
        root.style.setProperty("--primary-color-alpha", `${color}1A`);
      }
    };

    window.addEventListener("message", (event) =>
      handleColorChange(event.data)
    );
    return () => {
      client.disconnectUser();
      window.removeEventListener("message", (event) =>
        handleColorChange(event.data)
      );
    };
  }, []);

  useChecklist({ chatClient: client, targetOrigin: targetOrigin! });

  return (
    <>
      <div className="app__wrapper str-chat">
        <Chat {...{ client, i18nInstance }} theme={`team ${theme}`}>
          <WorkspaceController>
            <Sidebar />
            <ChannelContainer />
          </WorkspaceController>
        </Chat>
      </div>
    </>
  );
};

function LoginPage() {
  const { isLoading, error, sendRequest } = useHttp();
  const [userName, setUserName] = useState("");
  const navigate = useNavigate();
  useEffect(() => {
    const savedUserName = getUserInfo();
    const savedToken = getToken();
    if (savedUserName && savedToken) {
      setUserName(savedUserName);
      navigate("/team-chat", { state: { token: savedToken, userName } });
    }
    if (savedUserName) {
      setUserName(savedUserName);
    }
  }, []);

  const onUserNameChange = (e: any) => {
    setUserName(e.target.value);
  };

  const loginHandler = async () => {
    try {
      const baseUrl = "https://api.belo.im/login/";
      const url = baseUrl + `${userName}`;
      const result = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const data = await result.json();
      console.log("data", data.token);
      if (data.token) {
        user = userName;
        userToken = "Bearer " + data.token;
        saveToken(userToken);
        navigate("/team-chat");
        saveUserInfo(userName);
      }
    } catch (error) {
      notification.error({
        message: "fetch error",
        placement: "top",
      });
    }
  };

  return (
    <div
      style={{
        padding: "50px 16px",
        textAlign: "center",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Row>
        <>
          <Col span={24} style={{ marginBottom: 16 }}>
            <Input
              size="large"
              value={userName}
              onChange={onUserNameChange}
              placeholder={"Enter your name"}
              style={{ width: 500 }}
              onPressEnter={loginHandler}
            />
          </Col>
          <Col span={24} style={{ marginBottom: 16 }}>
            <Button
              size="large"
              type="primary"
              onClick={loginHandler}
              disabled={userName ? false : true}
              style={{ backgroundColor: "var(--primary-color)" }}
              loading={isLoading}
            >
              {"Login"}
            </Button>
          </Col>
        </>
      </Row>
    </div>
  );
}

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<LoginPage />} />
      <Route path="/team-chat" element={<ChatPage />} />
    </Routes>
  );
};

export default App;
