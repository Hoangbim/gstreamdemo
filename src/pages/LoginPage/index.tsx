import { Button, Col, Input, Row, notification } from "antd";
import React, { useEffect, useState } from "react";
// import jwt from "jsonwebtoken";

import { useNavigate } from "react-router-dom";

import {
  getUserInfo,
  saveUserInfo,
  useHttp,
  saveToken,
  getToken
} from "../../utils/useHttp";

function LoginPage() {
  const {isLoading, error, sendRequest} = useHttp();
  const [userName, setUserName] = useState("");
  // const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {
    const savedUserName = getUserInfo();
    const savedToken = getToken();
    if (savedUserName && savedToken) {
      // setIsLoggedIn(true);
      setUserName(savedUserName);
      navigate("/team-chat",{state: {token: savedToken, userName}});
    }
    if (savedUserName) {
      // setIsLoggedIn(true);
      setUserName(savedUserName);
    }
  }, []);


  const onUserNameChange = (e:any) => {
    setUserName(e.target.value);
  };

  const loginHandler =async () => {
   
  const baseUrl = 'http://42.119.181.15:8888/login/'
    const url = baseUrl + `${userName}`;
    console.log('-----------host url------------', url);
    const result = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    })
    const data = await result.json();
    console.log('data',data.token);

    if(data.token) {
      const token  = 'Barer ' + data.token;
      saveToken(token);
      navigate("/team-chat");
      saveUserInfo(userName);
    }else {
      console.log('----------fetch error----------', error);
      notification.error({
        message: error,
        placement: "top",
      });
    }
  };


  return (
    <div style={{ padding: "50px 16px", textAlign: "center",  display:"flex", justifyContent:'center' }}>
      <Row>
          <>
            <Col span={24} style={{ marginBottom: 16 }}>
              <Input
                size="large"
                value={userName}
                onChange={onUserNameChange}
                placeholder={'Enter your name'}
                style={{ width: 500 }}
              />
            </Col>
            <Col span={24} style={{ marginBottom: 16 }}>
              <Button
                size="large"
                type="primary"
                onClick={loginHandler}
                disabled={userName ? false : true}
                style={{ backgroundColor: "var(--primary-color)" }}
                loading={isLoading}
              >
                {'Login'}
              </Button>
            </Col>
          </>
        
      </Row>
    </div>
  );
}

export default LoginPage;
